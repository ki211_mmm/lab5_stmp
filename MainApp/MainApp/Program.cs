﻿using System;
using System.IO;
using System.Reflection;
using InterfaceLibrary;

namespace MainApp
{
    class Program
    {
        private static ICalculator calculator;
        private static Assembly calcAssembly;

        static void Main(string[] args)
        {
            LoadInterfaceDLL();
            while (true)
            {
                Console.WriteLine("Enter a command (load, unload, add, sub, mul, div, exit):");
                string command = Console.ReadLine();
                switch (command)
                {
                    case "load":
                        LoadCalculationDLL();
                        break;
                    case "unload":
                        UnloadCalculationDLL();
                        break;
                    case "add":
                        PerformOperation((a, b) => calculator.Add(a, b));
                        break;
                    case "sub":
                        PerformOperation((a, b) => calculator.Subtract(a, b));
                        break;
                    case "mul":
                        PerformOperation((a, b) => calculator.Multiply(a, b));
                        break;
                    case "div":
                        PerformOperation((a, b) => calculator.Divide(a, b));
                        break;
                    case "exit":
                        return;
                }
            }
        }

        static void LoadInterfaceDLL()
        {
            // Assuming InterfaceLibrary.dll is in the same directory as the executable
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "InterfaceLibrary.dll");
            Assembly.LoadFrom(path);
        }

        static void LoadCalculationDLL()
        {
            if (calcAssembly == null)
            {
                string path = @"E:\labs\СТМП\lab5\CalculationLibrary\CalculationLibrary\bin\Debug\net8.0\CalculationLibrary.dll";
                calcAssembly = Assembly.LoadFrom(path);
                Type calcType = calcAssembly.GetType("CalculationLibrary.Calculator");
                calculator = (ICalculator)Activator.CreateInstance(calcType);
                Console.WriteLine("Calculation DLL loaded.");
            }
            else
            {
                Console.WriteLine("Calculation DLL is already loaded.");
            }
        }

        static void UnloadCalculationDLL()
        {
            if (calcAssembly != null)
            {
                calculator = null;
                calcAssembly = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Console.WriteLine("Calculation DLL unloaded.");
            }
            else
            {
                Console.WriteLine("Calculation DLL is not loaded.");
            }
        }

        static void PerformOperation(Func<double, double, double> operation)
        {
            if (calculator == null)
            {
                Console.WriteLine("Calculation DLL is not loaded. Please load it first using the 'load' command.");
                return;
            }

            try
            {
                Console.Write("Enter first number: ");
                double a = double.Parse(Console.ReadLine());
                Console.Write("Enter second number: ");
                double b = double.Parse(Console.ReadLine());
                double result = operation(a, b);
                Console.WriteLine("Result: " + result);
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid input. Please enter valid numbers.");
            }
        }
    }
}
